<?php


namespace bobroid\skiddleApi\requests;


use bobroid\skiddleApi\entities\Event;

class Events extends BaseRequest
{



    public function getEndpoint(): string
    {
        return '/events/search';
    }

    public function getResponseEntityClass(): ?string
    {
        return Event::class;
    }

    public function searchByGeoCoords(float $latitude, float $longitude, int $radius): void
    {

    }
}