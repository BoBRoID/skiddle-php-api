<?php


namespace bobroid\skiddleApi\requests;


use bobroid\skiddleApi\entities\Venue;

class Venues extends BaseRequest
{

    /**
     * @inheritDoc
     */
    public function getEndpoint(): string
    {
        return '/venues';
    }

    public function getResponseEntityClass(): ?string
    {
        return Venue::class;
    }
}