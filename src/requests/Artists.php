<?php


namespace bobroid\skiddleApi\requests;


use bobroid\skiddleApi\entities\Artist;

class Artists extends BaseRequest
{

    /**
     * @inheritDoc
     */
    public function getEndpoint(): string
    {
        return '/artists';
    }

    public function getResponseEntityClass(): ?string
    {
        return Artist::class;
    }
}