<?php


namespace bobroid\skiddleApi\requests;


use bobroid\skiddleApi\filters\AbstractFilter;
use bobroid\skiddleApi\helpers\ArrayHelper;

abstract class BaseRequest
{

    public const ORDER_TRENDING = 'trending';
    public const ORDER_GOINGTO = 'goingto';
    public const ORDER_DISTANCE = 'distance';

    /**
     * @var null|AbstractFilter
     */
    private $filter;

    /**
     * @var float
     */
    private $order;

    /**
     * @var float
     */
    private $limit;

    /**
     * @var float
     */
    private $offset;

    /**
     * get api call query endpoint
     *
     * @return string
     */
    abstract public function getEndpoint(): string;

    /**
     * @param AbstractFilter|null $filter
     * @return $this
     */
    public function setFilter(?AbstractFilter $filter): self
    {
        $this->filter = $filter;

        return $this;
    }

    /**
     * @param string|null $order
     * @return $this
     */
    public function setOrder(?string $order): self
    {
        $this->order = $order;

        return $this;
    }

    /**
     * @param int|null $limit
     * @return $this
     * @throws \Exception
     */
    public function setLimit(?int $limit): self
    {
        if ($limit > 100) {
            $limit = 100;
        }

        $this->limit = $limit;

        return $this;
    }

    /**
     * @param int|null $offset
     * @return $this
     */
    public function setOffset(?int $offset): self
    {
        $this->offset = $offset;

        return $this;
    }

    /**
     * @return array
     */
    public function getQueryData(): array
    {
        return ArrayHelper::wash(
            array_merge([
                'order'     =>  $this->order,
                'limit'     =>  $this->limit,
                'offset'    =>  $this->offset,
            ], $this->filter ? $this->filter->getFields() : [])
        );
    }

    /**
     * @return string
     */
    public function getRequestMethod(): string
    {
        return 'GET';
    }

    /**
     * @return string|null
     */
    public function getResponseEntityClass(): ?string
    {
        return null;
    }

}