<?php


namespace bobroid\skiddleApi\filters;


use bobroid\skiddleApi\entities\Event;
use bobroid\skiddleApi\entities\GeoPoint;

/**
 * Class EventsFilter
 * @package bobroid\skiddleApi\filters
 *
 * @method self byCountry(?string $country)
 * @method self byKeyword(?string $keyword)
 * @method self byTicketsAvailable(?bool $ticketsAvailable)
 * @method self byImageFilter(?bool $imageFilter)
 * @method self byDescription(?bool $description)
 * @method self byUnder18(?bool $under18)
 * @method self byMinDate(?string $minDate) date in format YYYY-MM-DD
 * @method self byMaxDate(?string $maxDate) date in format YYYY-MM-DD
 * @method self byVenueId(?int $venueId)
 *
 */
class EventsFilter extends AbstractFilter
{

    /**
     * @return array
     */
    public static function getPossibleEventCodes(): array
    {
        return [
            Event::CODE_FEST    =>  'Festivals',
            Event::CODE_LIVE    =>  'Live music',
            Event::CODE_CLUB    =>  'Clubbing/Dance music',
            Event::CODE_DATE    =>  'Dating event',
            Event::CODE_THEATRE =>  'Theatre/Dance',
            Event::CODE_COMEDY  =>  'Comedy',
            Event::CODE_EXHIB   =>  'Exhibitions and Attractions',
            Event::CODE_KIDS    =>  'Kids/Family Event',
            Event::CODE_BARPUB  =>  'Bar/Pub event',
            Event::CODE_LGB     =>  'Gay/Lesbian event',
            Event::CODE_SPORT   =>  'Sporting event',
            Event::CODE_ARTS    =>  'The Arts'
        ];
    }

    /**
     * @param GeoPoint|null $geoPoint
     * @return $this
     */
    public function byGeoPoint(?GeoPoint $geoPoint = null): self
    {
        $this->fields = array_merge($this->fields, [
            'latitude'  =>  !$geoPoint ? null : $geoPoint->latitude,
            'longitude' =>  !$geoPoint ? null : $geoPoint->longitude,
            'radius'    =>  !$geoPoint ? null : $geoPoint->radius
        ]);

        return $this;
    }

    /**
     * @param string|null $eventCode
     * @return $this
     * @throws \Exception
     */
    public function byEventCode(?string $eventCode): self
    {
        if (!array_key_exists($eventCode, self::getPossibleEventCodes())) {
            throw new \Exception();
        }

        $this->fields['eventcode'] = $eventCode;

        return $this;
    }

    /**
     * @param bool|null $specialFeatured
     * @return $this
     */
    public function bySpecialFeatured(?bool $specialFeatured): self
    {
        $this->fields['specialFeatured'] = $specialFeatured;

        return $this;
    }
    
}