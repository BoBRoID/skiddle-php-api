<?php


namespace bobroid\skiddleApi\filters;


use bobroid\skiddleApi\helpers\ArrayHelper;

abstract class AbstractFilter
{

    protected $fields;

    public function __call(string $name, array $arguments)
    {
        $this->fields[strtolower($name)] = $arguments;

        return $this;
    }

    public function getFields(): array
    {
        return ArrayHelper::wash($this->fields);
    }

}