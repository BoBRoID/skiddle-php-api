<?php


namespace bobroid\skiddleApi;

use bobroid\skiddleApi\exceptions\UnexpectedRequestMethodException;
use bobroid\skiddleApi\responses\BaseResponse;
use bobroid\skiddleApi\requests\BaseRequest;
use GuzzleHttp\Client;

class Connection
{

    private const API_VER = 'v1';

    private const API_ENDPOINT = 'https://www.skiddle.com/api/';

    private const ALLOWED_REQUEST_METHODS = ['GET', 'DELETE', 'PUT', 'POST'];

    private $session;

    /**
     * Connection constructor.
     * @param Session $session
     */
    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    /**
     * @param BaseRequest $request
     * @return BaseResponse
     * @throws UnexpectedRequestMethodException
     * @throws exceptions\UnexpectedEntityException
     */
    public function performRequest(BaseRequest $request): BaseResponse
    {
        if (!\in_array($request->getRequestMethod(), self::ALLOWED_REQUEST_METHODS, true)) {
            throw new UnexpectedRequestMethodException();
        }

        $queryData = array_merge(['api_key' => $this->session->getApiKey()], $request->getQueryData());

        $endpoint = self::API_ENDPOINT . self::API_VER . $request->getEndpoint() . '?' . http_build_query($queryData);

        $client = new Client();
        $response = $client->request($request->getRequestMethod(), $endpoint);

        $decodedData = \json_decode($response->getBody(), true);

        return BaseResponse::createFromData($decodedData, $request->getResponseEntityClass());
    }

}