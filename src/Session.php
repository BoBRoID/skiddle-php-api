<?php


namespace bobroid\skiddleApi;


class Session
{

    private $apiKey;

    private $connection;

    /**
     * Session constructor.
     * @param string $apiKey
     * @param bool $debugMode
     */
    public function __construct(string $apiKey, bool $debugMode = false)
    {
        $this->apiKey = $apiKey;
    }

    /**
     * @return string
     */
    public function getApiKey(): string
    {
        return $this->apiKey;
    }

    /**
     * @return Connection
     */
    public function getConnection(): Connection
    {
        if ($this->connection === null) {
            $this->connection = new Connection($this);
        }

        return $this->connection;
    }

}