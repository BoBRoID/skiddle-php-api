<?php


namespace bobroid\skiddleApi\exceptions;


class UnexpectedEntityException extends \Exception
{

    protected $message = 'Unexpected entity! Excepted instance of `BaseEntity`!';

}