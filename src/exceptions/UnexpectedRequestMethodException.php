<?php


namespace bobroid\skiddleApi\exceptions;


class UnexpectedRequestMethodException extends \Exception
{

    protected $message = 'Unexpected request method!';

}