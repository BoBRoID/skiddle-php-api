<?php


namespace bobroid\skiddleApi\helpers;


class ArrayHelper
{

    public static function wash(array $array): array
    {
        return array_filter($array, function($field) {
            if (is_array($field)) return !empty(self::wash($field));

            return trim($field);
        });
    }

}