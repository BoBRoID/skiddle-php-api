<?php


namespace bobroid\skiddleApi\helpers;


use bobroid\skiddleApi\entities\BaseEntity;
use bobroid\skiddleApi\entities\MappableEntity;
use bobroid\skiddleApi\exceptions\UnexpectedEntityException;

class EntityHelper
{

    /**
     * create an entity from data (for example, server response)
     *
     * @param array $result
     * @param string $entityClass
     *
     * @return BaseEntity
     * @throws UnexpectedEntityException
     */
    public static function createFromData(array $result, string $entityClass): BaseEntity
    {
        /**
         * @var $entity BaseEntity
         */
        $entity = new $entityClass;

        if ($entity instanceof BaseEntity === false) {
            throw new UnexpectedEntityException();
        }

        if ($entity instanceof MappableEntity) {
            $result = self::mapDataToEntityFields($result, $entity::getFieldsMap());
        }

        $entity->setFields($result);

        return $entity;
    }


    /**
     * @param array $data - a result received in response
     * @param array $fieldsMap - fields map (see MappableEntity for example)
     *
     * @return array
     * @throws UnexpectedEntityException
     */
    public static function mapDataToEntityFields(array $data, array $fieldsMap): array
    {
        $mappedResult = [];

        foreach ($data as $responseFieldName => $responseValue) {
            $newFieldName = $responseFieldName;

            if (array_key_exists($responseFieldName, $fieldsMap)) {
                $newFieldName = $fieldsMap[$responseFieldName];

                if (is_array($newFieldName)) {
                    [$newFieldName, $newFieldClass] = $newFieldName;

                    $responseValue = self::createFromData($responseValue, $newFieldClass);
                }
            }

            $mappedResult[$newFieldName] = $responseValue;
        }

        return $mappedResult;
    }

}