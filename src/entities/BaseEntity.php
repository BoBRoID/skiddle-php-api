<?php


namespace bobroid\skiddleApi\entities;


abstract class BaseEntity
{

    public function setFields(array $fields): void
    {
        $entityFields = get_object_vars($this);

        foreach ($entityFields as $field => $fieldValue) {
            if (!array_key_exists($field, $fields)) {
                continue;
            }

            $this->$field = $fields[$field];
        }
    }

}