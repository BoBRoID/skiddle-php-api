<?php


namespace bobroid\skiddleApi\entities;


interface MappableEntity
{

    /**
     * @return array - array of fields map
     * @example [ responseFieldName => entityFieldName ] or [ responseFieldName => [ entityFieldName, entityClass ] ]
     */
    public static function getFieldsMap(): array;

}