<?php


namespace bobroid\skiddleApi\entities;


class OpeningTimes extends BaseEntity implements MappableEntity
{

    public $doorsOpen;

    public $doorsClose;

    public $lastEntry;

    /**
     * @inheritDoc
     */
    public static function getFieldsMap(): array
    {
        return [
            'doorsopen'     =>  'doorsOpen',
            'doorsclose'    =>  'doorsClose',
            'lastentry'     =>  'lastEntry'
        ];
    }
}