<?php


namespace bobroid\skiddleApi\entities;


class GeoPoint extends BaseEntity
{

    public $latitude;

    public $longitude;

    public $radius;

    public function __construct(array $data)
    {
        $this->setFields($data);
    }
}