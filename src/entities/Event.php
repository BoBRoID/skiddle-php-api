<?php

namespace bobroid\skiddleApi\entities;

class Event extends BaseEntity implements MappableEntity
{

    public const CODE_FEST = 'FEST';
    public const CODE_LIVE = 'LIVE';
    public const CODE_CLUB = 'CLUB';
    public const CODE_DATE = 'DATE';
    public const CODE_THEATRE = 'THEATRE';
    public const CODE_COMEDY = 'COMEDY';
    public const CODE_EXHIB = 'EXHIB';
    public const CODE_KIDS = 'KIDS';
    public const CODE_BARPUB = 'BARPUB';
    public const CODE_LGB = 'LGB';
    public const CODE_SPORT = 'SPORT';
    public const CODE_ARTS = 'ARTS';

    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $eventCode;

    /**
     * @var string
     */
    public $eventName;

    /**
     * @var int|bool
     */
    public $cancelled;

    /**
     * @var Venue
     */
    public $venue;

    /**
     * @var string
     */
    public $imageUrl;

    /**
     * @var string
     */
    public $largeImageUrl;

    /**
     * @var string
     */
    public $link;

    /**
     * @var string
     *
     * date in `Y-m-d` format
     */
    public $date;

    /**
     * @var string
     *
     * date in full format (with timezone)
     */
    public $startDate;

    /**
     * @var string
     *
     * date in full format (with timezone)
     */
    public $endDate;

    /**
     * @var string
     */
    public $description;

    /**
     * @var int
     *
     * minimal age for this event
     */
    public $minAge;

    /**
     * @var unknown
     *
     * define that type
     */
    public $imgoing;

    /**
     * @var int
     *
     * define that type
     */
    public $gointos;

    /**
     * @var int
     *
     * I guess that is amount of crowd which will go to this event
     */
    public $goingToCount;

    /**
     * @var boolean
     */
    public  $tickets;

    /**
     * @var string
     *
     * description of entry price
     */
    public  $entryPrice;

    /**
     * @var string
     *
     * `public`, for example
     */
    public  $eventVisibility;

    /**
     * @var object
     *
     * something, define that field
     */
    public  $rep;

    /**
     * @var OpeningTimes
     */
    public  $openingTimes;

    public static function getFieldsMap(): array
    {
        return [
            'EventCode'         =>  'eventCode',
            'eventname'         =>  'eventName',
            'venue'             =>  ['venue', Venue::class],
            'imageurl'          =>  'imageUrl',
            'largeimageurl'     =>  'largeImageUrl',
            'startdate'         =>  'startDate',
            'enddate'           =>  'endDate',
            'openingtimes'      =>  ['openingTimes', OpeningTimes::class],
            'minage'            =>  'minAge',
            'goingtocount'      =>  'goingToCount',
            'entryprice'        =>  'entryPrice',
            'eventvisibility'   =>  'eventVisibility'
        ];
    }
}