<?php


namespace bobroid\skiddleApi\entities;


class Venue extends BaseEntity implements MappableEntity
{
    /*
     ["venue"]=>
      array(12) {
        ["id"]=>
        int(343)
        ["name"]=>
        string(6) "Fabric"
        ["address"]=>
        string(23) "77a Charterhouse Street"
        ["town"]=>
        string(6) "London"
        ["postcode_lookup"]=>
        string(6) "London"
        ["postcode"]=>
        string(8) "EC1M 6HJ"
        ["country"]=>
        string(2) "GB"
        ["phone"]=>
        string(13) "0207 336 8898"
        ["latitude"]=>
        float(51.519585)
        ["longitude"]=>
        float(-0.102474)
        ["type"]=>
        string(9) "Nightclub"
        ["rating"]=>
        int(4)
      }
     */

    public  $id;
    public  $name;
    public  $address;
    public  $town;
    public  $postcodeLookup;
    public  $postcode;
    public  $country;
    public  $phone;
    public  $latitude;
    public  $longitude;
    public  $type;
    public  $rating;

    /**
     * @inheritDoc
     */
    public static function getFieldsMap(): array
    {
        return [
            'postcode_lookup'   =>  'postcodeLookup',
        ];
    }
}