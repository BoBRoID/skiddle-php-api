<?php


namespace bobroid\skiddleApi\entities;


class Artist extends BaseEntity implements MappableEntity
{

    public $id;

    public $name;

    public $imageUrl;

    public $nextEvent;

    public $favourite;

    public $spotifyMP3Url;

    public $spotifyArtistUrl;

    /**
     * @inheritDoc
     */
    public static function getFieldsMap(): array
    {
        return [
            'nextevent'         =>  'nextEvent',
            'imageurl'          =>  'imageUrl',
            'spotifymp3url'     =>  'spotifyMP3Url',
            'spotifyartisturl'  =>  'spotifyArtistUrl'
        ];
    }
}