<?php


namespace bobroid\skiddleApi\responses;


use bobroid\skiddleApi\helpers\EntityHelper;

class BaseResponse
{

    public  $error;

    public  $totalCount;

    public  $pageCount;

    private $results = [];

    public function __construct(array $fields)
    {
        foreach ($fields as $fieldName => $fieldValue) {
            if (!property_exists($this, $fieldName)) {
                continue;
            }

            $this->$fieldName = $fieldValue;
        }
    }

    public function getResults(): array
    {
        return $this->results;
    }

    /**
     * creating response from response data
     *
     * @param array $response
     * @param string|null $entityClass
     * @return static
     * @throws \bobroid\skiddleApi\exceptions\UnexpectedEntityException
     */
    public static function createFromData(array $response, ?string $entityClass = null): self
    {
        $resultEntities = [];

        if ($entityClass) {
            $responseEntities = $response['results'] ?? [];

            foreach ($responseEntities as $responseEntity) {
                $resultEntities[] = EntityHelper::createFromData($responseEntity, $entityClass);
            }
        }

        return new static([
            'error'         =>  $response['error'] ?? null,
            'totalCount'    =>  $response['totalcount'] ?? null,
            'pageCount'     =>  $response['pagecount'] ?? null,
            'results'       =>  $resultEntities
        ]);
    }

}